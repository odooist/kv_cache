===============
Key Value Cache
===============

Features
--------
Sometimes it's required to keep a process state across multiple requests.

In this case kv_cache module can be used.

Usage
-----
See source code for usage.

Issues & Development
--------------------
Visit the `project page <http://gitlab.com/odooist/kv_cache>`_ at Gitlab.

