# -*- encoding: utf-8 -*-
{
    'name': 'Key / value cache',
    'version': '1.1',
    'author': 'Odooist',
    'maintainer': 'Odooist',
    'support': 'odooist@gmail.com',
    'license': 'LGPL-3',
    'category': 'Tools',
    'summary': 'Odoo key / value cache implementation',
    'description': '',
    'depends': [],
    'data': [
        'security/ir.model.access.csv',
    ],
    'demo': [],
    "qweb": ['static/src/xml/*.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': ['static/description/icon.png'],
}
